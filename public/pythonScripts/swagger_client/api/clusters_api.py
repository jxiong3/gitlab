# coding: utf-8

"""
    ModelCenter Cloud Swagger Client Library

    This is a test client library for ModelCenter Cloud. You can find out more about [ModelCenter Cloud Server API](https://10.10.14.75/execution/mcs/help.html) .        # noqa: E501

    OpenAPI spec version: 1.0.0
    Contact: jxiong@phoenix-int.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from swagger_client.api_client import ApiClient


class ClustersApi(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def clusters_by_cluster_id_get(self, cluster_id, **kwargs):  # noqa: E501
        """getCluster  # noqa: E501

        Fetch a cluster, by ID  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.clusters_by_cluster_id_get(cluster_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str cluster_id:  (required)
        :param str fields: List of fields to provide as a partial response.
        :return: IClusterModel
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.clusters_by_cluster_id_get_with_http_info(cluster_id, **kwargs)  # noqa: E501
        else:
            (data) = self.clusters_by_cluster_id_get_with_http_info(cluster_id, **kwargs)  # noqa: E501
            return data

    def clusters_by_cluster_id_get_with_http_info(self, cluster_id, **kwargs):  # noqa: E501
        """getCluster  # noqa: E501

        Fetch a cluster, by ID  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.clusters_by_cluster_id_get_with_http_info(cluster_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str cluster_id:  (required)
        :param str fields: List of fields to provide as a partial response.
        :return: IClusterModel
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['cluster_id', 'fields']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method clusters_by_cluster_id_get" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'cluster_id' is set
        if ('cluster_id' not in params or
                params['cluster_id'] is None):
            raise ValueError("Missing the required parameter `cluster_id` when calling `clusters_by_cluster_id_get`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'cluster_id' in params:
            path_params['clusterId'] = params['cluster_id']  # noqa: E501

        query_params = []
        if 'fields' in params:
            query_params.append(('fields', params['fields']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/vnd.phx.mcs-v1+json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Secured']  # noqa: E501

        return self.api_client.call_api(
            '/clusters/{clusterId}', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='IClusterModel',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def clusters_get(self, **kwargs):  # noqa: E501
        """getClusters  # noqa: E501

        Fetch multiple clusters  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.clusters_get(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int offset: Offset from the first item to fetch.
        :param int limit: The maximum number of items to fetch.
        :param str fields: List of fields to provide as a partial response.
        :param str q: List of search terms.
        :param list[str] filters: List of filters to apply; for help with filters, click <a href=\"../mcs/howTo_Filters.html\">here</a>.
        :return: list[IClusterModel]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.clusters_get_with_http_info(**kwargs)  # noqa: E501
        else:
            (data) = self.clusters_get_with_http_info(**kwargs)  # noqa: E501
            return data

    def clusters_get_with_http_info(self, **kwargs):  # noqa: E501
        """getClusters  # noqa: E501

        Fetch multiple clusters  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.clusters_get_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int offset: Offset from the first item to fetch.
        :param int limit: The maximum number of items to fetch.
        :param str fields: List of fields to provide as a partial response.
        :param str q: List of search terms.
        :param list[str] filters: List of filters to apply; for help with filters, click <a href=\"../mcs/howTo_Filters.html\">here</a>.
        :return: list[IClusterModel]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['offset', 'limit', 'fields', 'q', 'filters']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method clusters_get" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []
        if 'offset' in params:
            query_params.append(('offset', params['offset']))  # noqa: E501
        if 'limit' in params:
            query_params.append(('limit', params['limit']))  # noqa: E501
        if 'fields' in params:
            query_params.append(('fields', params['fields']))  # noqa: E501
        if 'q' in params:
            query_params.append(('q', params['q']))  # noqa: E501
        if 'filters' in params:
            query_params.append(('filters', params['filters']))  # noqa: E501
            collection_formats['filters'] = 'multi'  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/vnd.phx.mcs-v1+json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Secured']  # noqa: E501

        return self.api_client.call_api(
            '/clusters', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='list[IClusterModel]',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def clusters_queues_by_cluster_id_and_queue_id_get(self, cluster_id, queue_id, **kwargs):  # noqa: E501
        """getQueue  # noqa: E501

        Fetch a queue, by ID  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.clusters_queues_by_cluster_id_and_queue_id_get(cluster_id, queue_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str cluster_id:  (required)
        :param str queue_id:  (required)
        :param str fields: List of fields to provide as a partial response.
        :return: IQueueModel
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.clusters_queues_by_cluster_id_and_queue_id_get_with_http_info(cluster_id, queue_id, **kwargs)  # noqa: E501
        else:
            (data) = self.clusters_queues_by_cluster_id_and_queue_id_get_with_http_info(cluster_id, queue_id, **kwargs)  # noqa: E501
            return data

    def clusters_queues_by_cluster_id_and_queue_id_get_with_http_info(self, cluster_id, queue_id, **kwargs):  # noqa: E501
        """getQueue  # noqa: E501

        Fetch a queue, by ID  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.clusters_queues_by_cluster_id_and_queue_id_get_with_http_info(cluster_id, queue_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str cluster_id:  (required)
        :param str queue_id:  (required)
        :param str fields: List of fields to provide as a partial response.
        :return: IQueueModel
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['cluster_id', 'queue_id', 'fields']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method clusters_queues_by_cluster_id_and_queue_id_get" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'cluster_id' is set
        if ('cluster_id' not in params or
                params['cluster_id'] is None):
            raise ValueError("Missing the required parameter `cluster_id` when calling `clusters_queues_by_cluster_id_and_queue_id_get`")  # noqa: E501
        # verify the required parameter 'queue_id' is set
        if ('queue_id' not in params or
                params['queue_id'] is None):
            raise ValueError("Missing the required parameter `queue_id` when calling `clusters_queues_by_cluster_id_and_queue_id_get`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'cluster_id' in params:
            path_params['clusterId'] = params['cluster_id']  # noqa: E501
        if 'queue_id' in params:
            path_params['queueId'] = params['queue_id']  # noqa: E501

        query_params = []
        if 'fields' in params:
            query_params.append(('fields', params['fields']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/vnd.phx.mcs-v1+json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Secured']  # noqa: E501

        return self.api_client.call_api(
            '/clusters/{clusterId}/queues/{queueId}', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='IQueueModel',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def clusters_queues_by_cluster_id_get(self, cluster_id, **kwargs):  # noqa: E501
        """getQueues  # noqa: E501

        Fetch multiple queues for the cluster specified  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.clusters_queues_by_cluster_id_get(cluster_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str cluster_id:  (required)
        :param int offset: Offset from the first item to fetch.
        :param int limit: The maximum number of items to fetch.
        :param str fields: List of fields to provide as a partial response.
        :param str q: List of search terms.
        :param list[str] filters: List of filters to apply; for help with filters, click <a href=\"../mcs/howTo_Filters.html\">here</a>.
        :return: list[IQueueModel]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.clusters_queues_by_cluster_id_get_with_http_info(cluster_id, **kwargs)  # noqa: E501
        else:
            (data) = self.clusters_queues_by_cluster_id_get_with_http_info(cluster_id, **kwargs)  # noqa: E501
            return data

    def clusters_queues_by_cluster_id_get_with_http_info(self, cluster_id, **kwargs):  # noqa: E501
        """getQueues  # noqa: E501

        Fetch multiple queues for the cluster specified  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.clusters_queues_by_cluster_id_get_with_http_info(cluster_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str cluster_id:  (required)
        :param int offset: Offset from the first item to fetch.
        :param int limit: The maximum number of items to fetch.
        :param str fields: List of fields to provide as a partial response.
        :param str q: List of search terms.
        :param list[str] filters: List of filters to apply; for help with filters, click <a href=\"../mcs/howTo_Filters.html\">here</a>.
        :return: list[IQueueModel]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['cluster_id', 'offset', 'limit', 'fields', 'q', 'filters']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method clusters_queues_by_cluster_id_get" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'cluster_id' is set
        if ('cluster_id' not in params or
                params['cluster_id'] is None):
            raise ValueError("Missing the required parameter `cluster_id` when calling `clusters_queues_by_cluster_id_get`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'cluster_id' in params:
            path_params['clusterId'] = params['cluster_id']  # noqa: E501

        query_params = []
        if 'offset' in params:
            query_params.append(('offset', params['offset']))  # noqa: E501
        if 'limit' in params:
            query_params.append(('limit', params['limit']))  # noqa: E501
        if 'fields' in params:
            query_params.append(('fields', params['fields']))  # noqa: E501
        if 'q' in params:
            query_params.append(('q', params['q']))  # noqa: E501
        if 'filters' in params:
            query_params.append(('filters', params['filters']))  # noqa: E501
            collection_formats['filters'] = 'multi'  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/vnd.phx.mcs-v1+json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Secured']  # noqa: E501

        return self.api_client.call_api(
            '/clusters/{clusterId}/queues', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='list[IQueueModel]',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)
