import zipfile
import tarfile
import sys
import os


# replace the last . into _
def createPczName(pczFolderName):
   head, _sep, tail = pczFolderName.rpartition('_')
   return head + '.' + tail

def getFolderFiles(members, paczFolderName, paczArchivePrefix):
   n = len(paczArchivePrefix)
   for tarinfo in members:
      if tarinfo.name.startswith(paczArchivePrefix):
         tarinfo.path = paczFolderName + '/' + tarinfo.path[n:]
         yield tarinfo

def getParentPath(path):
   head, _sep, tail = path.rpartition('/')
   return head

def zip(src, dst):
        abs_src = os.path.abspath(src)
        if os.path.exists(abs_src):
                zf = zipfile.ZipFile("%s" % (dst), "w", zipfile.ZIP_DEFLATED)
                for dirname, subdirs, files in os.walk(src):
                        for filename in files:
                                absname = os.path.abspath(os.path.join(dirname, filename))
                                arcname = absname[len(abs_src) + 1:]
                                zf.write(absname, arcname)
                zf.close()
        else:
                print("The path %s does not exist." % os.path.abspath(dst))

# unzip the zip file and extract the folder with the pacz then zip it again
def extractPaczToZip(zipFilePath, paczFolderName, paczArchivePrefix):
   tar = tarfile.open(zipFilePath)
   parentPath = getParentPath(zipFilePath )
   tar.extractall(parentPath,members=getFolderFiles(tar.getmembers(), paczFolderName, paczArchivePrefix)) # extract the pacz folder
   tar.close()
   zip(parentPath + "/" + paczFolderName, parentPath + "/" + createPczName(paczFolderName)) # zip the pacz folder
		
