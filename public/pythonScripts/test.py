from flask import Flask, render_template, redirect, url_for,request
from flask import make_response
import gitlab  # use gitlab python API
import tarfile # extract tar file into some 
from flask_cors import CORS
from os.path import expanduser
from workflowVerification import jobverification 
from unzipToPacz import *

app = Flask(__name__)
CORS(app) # handling Cross Origin Resource Sharing (CORS); refer: https://flask-cors.readthedocs.io/en/latest/

gl = gitlab.Gitlab('http://10.10.9.59:3000', private_token='6xasgdHs-usknmcXBbvo')
projects = gl.projects.list()

home = expanduser("~")
@app.route("/")
def home():
    return "Welcome to the Python flask web server."

@app.route("/check")
def check():
   return "Check page."

@app.route('/jobVerification', methods=['POST', 'GET'])
def jobVerification():
   if request.method == 'POST':
        inputdir = request.form['inputdir']
	outputdir = request.form['outputdir']
	readmedir = request.form['readmedir']
	verificationdir = request.form['verificationdir']
	htmldir = request.form['htmldir']
        table_html = jobverification(inputdir,outputdir,readmedir,verificationdir,htmldir)

   return table_html

@app.route('/downloadPacz', methods=['POST', 'GET'])
def downloadPacz():
   requestProject = None
   paczArchivePath = None
   if request.method == 'POST':
        projectName = request.form['projectName']
	path = request.form['path']
        ref = request.form['ref']
        paczArchivePath = request.form['paczArchivePath']
        for project in projects:
           if project.name == projectName:
              requestProject = project        
        if requestProject:
           tgz = requestProject.repository_archive(sha=ref)  # achive the file into server cache folder
           shaId = requestProject.commits.list(ref_name=ref)[0].id
           paczArchiveName = projectName.lower() + "-" + ref + "-" + shaId
           paczArchivePrefix = paczArchiveName + "/" + path + "/"
           paczArchivePath = expanduser("~/gitlab-development-kit/gitlab/shared/cache/archive/project-" + str(requestProject.id) + "/" + shaId + "/" + paczArchiveName + ".tar.gz")
           
           extractPaczToZip(paczArchivePath, path, paczArchivePrefix)
           #if tarfile.is_tarfile(tgz):
              #tgz.extractall(paczArchivePath)   requestProject.commits.list(ref_name=ref)
   if requestProject:  # paczArchivePath.exists()
      return str(request.form['paczArchivePath'])
   else:
      return "There is nothing been returned."


if __name__ == "__main__":
    app.run(host = '10.10.9.59', debug = True)
