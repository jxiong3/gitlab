import json
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
import collections 
import urllib3
from prettytable import PrettyTable
import animation
import time
import markdown
from ipywidgets import widgets
from IPython.display import clear_output
urllib3.disable_warnings()

def jobVerification(inputdir,outputdir,readmedir,verificationdir,htmldir):
    # Configure HTTP basic authorization: Secured
    configuration = swagger_client.Configuration()
    configuration.username = 'Administrator'
    configuration.password = 'W1z8aNg!'
    configuration.verify_ssl = False
    cluster = 'Round Robin Connector'
    queue = 'default'
    name = 'workflowVerification'
    
    #Import software versions required
    f = open(readmedir, 'r')
    htmlmarkdown=markdown.markdown( f.read() )
    htmlmarkdown=htmlmarkdown.replace('<p>','')
    htmlmarkdown=htmlmarkdown.replace('</p>','')
    htmlmarkdown=htmlmarkdown.replace('\n','/')
    htmlmarkdown=htmlmarkdown.split('/')
    softwareVersionStart = 0
    for elements in htmlmarkdown:
        if elements == "Software Requirements:":
            softwareVersionStart=htmlmarkdown.index(elements)
    softwareNeeded=htmlmarkdown[softwareVersionStart+1:]
    softwareRequirements={}
    for elements in softwareNeeded:
        softwareRequirements[elements.split(':')[0]]=elements.split(': ')[1]
    
    #import input.txt
    with open(inputdir) as json_file:
        data = json.load(json_file)
    
    workflow_uri = data['workflowuri']
    
    testInputs=data['inputs']
    testOutputs=data['outputs']
    
    workflow_instance = swagger_client.WorkflowApi(swagger_client.ApiClient(configuration))
    workflow_data = workflow_instance.workflow_get(workflow_uri)
    
    #Get Input variables
    inputs = workflow_data.metadata.inputs
    input_variables = []
    for variables in inputs:
        inputVariable = {'name': variables.name, 'value': testInputs[variables.name], 'length': len(testInputs[variables.name].split(','))}
        input_variables.append(inputVariable)
    
    
    #Get Ouput variables
    outputs = workflow_data.metadata.outputs
    output_variables = []
    for variables in outputs:
        outputVariable = {'name': variables.name}
        output_variables.append(outputVariable)
    
    
    #Setting up trade study configuration 
    job_variables = []
    for variables in input_variables:
        job_variable = {'type': 'DESIGN',
                        'values' : variables['value'].split(','),
                        'variableId' : variables['name']}
        job_variables.append(job_variable)
        
    for variables in output_variables:
        job_variable = {'type' : 'RESPONSE',
                        'values' : [],
                        'variableId' : variables['name']}
        job_variables.append(job_variable)
        
    #Setting up tradeStudy Setup
    designVars = []
    for variables in input_variables:
        designVar = {'name': variables['name'],
                        "lBound":str(variables['value'][0]),
                        "uBound":str(variables['value'][-1]),
                        "values":[variables['length']],
                        "rangeType":"values"}
        designVars.append(designVar)
        
    responseVars = []
    for variables in output_variables:
        responseVar = {'name' : variables}
        responseVars.append(responseVar)
        
    #Setting up trade study design
    design ={}
    design = {"type" : "factorial","numLevels": designVars[0]['values']}
    trade_study_configuration = {"jobVariables": job_variables,
                                "initialVariables":[],
                                "tradeStudyType":"",
                                "tradeStudySetup":{"designVars": designVars,
                                                    "responses": responseVars,
                                                    "design": design}}
                                
    job_instance = swagger_client.JobsApi(swagger_client.ApiClient(configuration))
    job_submission_model={'name': name, 'cluster' : cluster, 'queue' : queue, 'tradeStudyConfiguration' : trade_study_configuration, 'workflowUri' : workflow_uri}
    
    
    
    def waitJob(timeOut, jobId):
        endTime = timeOut + time.time()
        while(1):
            getJob = job_instance.jobs_by_id_get(id = jobId)
            if (getJob.status  == "PENDING"):
                continue
            elif (getJob.status  == "CANCELLED"):
                print("The job already been cancelled.")
                return
            elif (getJob.status  == "SUCCESS"):
                #clear_output()
                print("Job has finished running")
                print("Results of the workflow verification have been posted to the files.")
                
                printMCSResult(getJob.data_sets[0].variables, getJob.completed_run_count, testOutputs)
                return
            
            currentTime = time.time()
            if (currentTime >= endTime):
                print("Time out.")
                return
            
            
    def printMCSResult(variables, totalCount, testOutputs):
        inputList = collections.defaultdict(list)
        outputList = collections.defaultdict(list)
        resultTable = PrettyTable()
        
        for variable in variables:
            if variable.type == "DESIGN":
                inputList[variable.variable_id] = variable.values
                length = len(variable.values)
            elif variable.type == "RESPONSE":
                outputList[variable.variable_id] = variable.values
    
        
        logIdColumn = []
        outputSepColumn = []
        for i in range(totalCount):
            logIdColumn.append(i)
            outputSepColumn.append("")
        resultTable.add_column("Run ID", logIdColumn)
    
    
        for each in inputList:
            column = []
            for i in range(totalCount):
                column.append(inputList[each][i].value)
            resultTable.add_column(each,column)
        resultTable.add_column("Output", outputSepColumn)
        for each in outputList:
            column = []
            for i in range(totalCount):
                column.append(outputList[each][i].value)
            resultTable.add_column(each, column)
            
        expectedOutputs = {}
        for variables in testOutputs:
            testValues=[]
            for values in testOutputs[variables].split(","):
                testValues.append(values)
            expectedOutputs[variables]=testValues
        
        
        resultTable.add_column("Expected Output", outputSepColumn)   
        for each in testOutputs:
            column = []
            for i in range(totalCount):
                column.append(expectedOutputs[each][i])
            resultTable.add_column(each, column)
            
        resultTable.add_column("Result Verification", outputSepColumn)
        for each in testOutputs:
            column = []
            for i in range(totalCount):
                column.append(expectedOutputs[each][i]==outputList[each][i].value)
            resultTable.add_column(each, column)
        
        verification={}
        for variables in expectedOutputs:
            verificationByVariable=[]
            i=0
            while i < len(expectedOutputs[variables]):
                verificationByVariable.append(expectedOutputs[variables][i]==outputList[variables][i].value)
                i += 1
            verification[variables]=verificationByVariable
                            
        
        
        with open(verificationdir, 'w') as outfile:
            json.dump(verification, outfile)
        
        
        table_txt = resultTable.get_string()
        with open(outputdir,'w') as file:
            file.write(table_txt)
            
        table_html = resultTable.get_html_string()
        with open(htmldir,'w') as file:
            file.write(table_html)
    
    
    try:
        createJob = job_instance.jobs_post(job_submission_model)
        setState = job_instance.jobs_state_by_id_put(body  = swagger_client.IJobStateModel(state = "START"), id = createJob.id)
        print("Job is being executed. Results will be displayed shortly.")
        waitJob(60, createJob.id)
        
    
    except ApiException as e:
       print("Exception when calling JobsApi->jobs_post: %s\n" % e)
