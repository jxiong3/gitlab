# coding: utf-8

"""
    ModelCenter Cloud Swagger Client Library

    This is a test client library for ModelCenter Cloud. You can find out more about [ModelCenter Cloud Server API](https://10.10.14.75/execution/mcs/help.html) .        # noqa: E501

    OpenAPI spec version: 1.0.0
    Contact: jxiong@phoenix-int.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from swagger_client.api_client import ApiClient


class WorkflowApi(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def workflow_get(self, workflow_uri, **kwargs):  # noqa: E501
        """getWorkflow  # noqa: E501

        Fetch information about an workflow  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.workflow_get(workflow_uri, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str workflow_uri: The URI of the workflow to interrogate for information (required)
        :param str fields: List of fields to provide as a partial response.
        :return: IWorkflowModel
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.workflow_get_with_http_info(workflow_uri, **kwargs)  # noqa: E501
        else:
            (data) = self.workflow_get_with_http_info(workflow_uri, **kwargs)  # noqa: E501
            return data

    def workflow_get_with_http_info(self, workflow_uri, **kwargs):  # noqa: E501
        """getWorkflow  # noqa: E501

        Fetch information about an workflow  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.workflow_get_with_http_info(workflow_uri, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str workflow_uri: The URI of the workflow to interrogate for information (required)
        :param str fields: List of fields to provide as a partial response.
        :return: IWorkflowModel
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['workflow_uri', 'fields']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method workflow_get" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'workflow_uri' is set
        if ('workflow_uri' not in params or
                params['workflow_uri'] is None):
            raise ValueError("Missing the required parameter `workflow_uri` when calling `workflow_get`")  # noqa: E501

        collection_formats = {}

        path_params = {}

        query_params = []
        if 'workflow_uri' in params:
            query_params.append(('workflowUri', params['workflow_uri']))  # noqa: E501
        if 'fields' in params:
            query_params.append(('fields', params['fields']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/vnd.phx.mcs-v1+json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Secured']  # noqa: E501

        return self.api_client.call_api(
            '/workflow', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='IWorkflowModel',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def workflow_icon_get(self, workflow_uri, **kwargs):  # noqa: E501
        """getWorkflowIcon  # noqa: E501

        Fetch the icon of a workflow. Implementation Notes Returns the icon's binary file data. To get the workflow's header and metadata see GET /workflow.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.workflow_icon_get(workflow_uri, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str workflow_uri: The URI of the workflow to interrogate for information (required)
        :return: object
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.workflow_icon_get_with_http_info(workflow_uri, **kwargs)  # noqa: E501
        else:
            (data) = self.workflow_icon_get_with_http_info(workflow_uri, **kwargs)  # noqa: E501
            return data

    def workflow_icon_get_with_http_info(self, workflow_uri, **kwargs):  # noqa: E501
        """getWorkflowIcon  # noqa: E501

        Fetch the icon of a workflow. Implementation Notes Returns the icon's binary file data. To get the workflow's header and metadata see GET /workflow.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.workflow_icon_get_with_http_info(workflow_uri, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str workflow_uri: The URI of the workflow to interrogate for information (required)
        :return: object
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['workflow_uri']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method workflow_icon_get" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'workflow_uri' is set
        if ('workflow_uri' not in params or
                params['workflow_uri'] is None):
            raise ValueError("Missing the required parameter `workflow_uri` when calling `workflow_icon_get`")  # noqa: E501

        collection_formats = {}

        path_params = {}

        query_params = []
        if 'workflow_uri' in params:
            query_params.append(('workflowUri', params['workflow_uri']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/octet-stream'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Secured']  # noqa: E501

        return self.api_client.call_api(
            '/workflow/icon', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='object',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)
