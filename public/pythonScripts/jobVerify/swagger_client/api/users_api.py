# coding: utf-8

"""
    ModelCenter Cloud Swagger Client Library

    This is a test client library for ModelCenter Cloud. You can find out more about [ModelCenter Cloud Server API](https://10.10.14.75/execution/mcs/help.html) .        # noqa: E501

    OpenAPI spec version: 1.0.0
    Contact: jxiong@phoenix-int.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from swagger_client.api_client import ApiClient


class UsersApi(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def users_by_user_name_get(self, user_name, **kwargs):  # noqa: E501
        """getUser  # noqa: E501

        Fetch a user, by username  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.users_by_user_name_get(user_name, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str user_name: The user's username (required)
        :param str fields: List of fields to provide as a partial response.
        :return: IUserModel
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.users_by_user_name_get_with_http_info(user_name, **kwargs)  # noqa: E501
        else:
            (data) = self.users_by_user_name_get_with_http_info(user_name, **kwargs)  # noqa: E501
            return data

    def users_by_user_name_get_with_http_info(self, user_name, **kwargs):  # noqa: E501
        """getUser  # noqa: E501

        Fetch a user, by username  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.users_by_user_name_get_with_http_info(user_name, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str user_name: The user's username (required)
        :param str fields: List of fields to provide as a partial response.
        :return: IUserModel
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['user_name', 'fields']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method users_by_user_name_get" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'user_name' is set
        if ('user_name' not in params or
                params['user_name'] is None):
            raise ValueError("Missing the required parameter `user_name` when calling `users_by_user_name_get`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'user_name' in params:
            path_params['userName'] = params['user_name']  # noqa: E501

        query_params = []
        if 'fields' in params:
            query_params.append(('fields', params['fields']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/vnd.phx.mcs-v1+json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Secured']  # noqa: E501

        return self.api_client.call_api(
            '/users/{userName}', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='IUserModel',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def users_get(self, **kwargs):  # noqa: E501
        """getUsers  # noqa: E501

        Fetch multiple users  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.users_get(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int offset: Offset from the first item to fetch.
        :param int limit: The maximum number of items to fetch.
        :param str fields: List of fields to provide as a partial response.
        :param str q: List of search terms.
        :param list[str] filters: List of filters to apply; for help with filters, click <a href=\"../mcs/howTo_Filters.html\">here</a>.
        :return: list[IUserModel]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.users_get_with_http_info(**kwargs)  # noqa: E501
        else:
            (data) = self.users_get_with_http_info(**kwargs)  # noqa: E501
            return data

    def users_get_with_http_info(self, **kwargs):  # noqa: E501
        """getUsers  # noqa: E501

        Fetch multiple users  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.users_get_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int offset: Offset from the first item to fetch.
        :param int limit: The maximum number of items to fetch.
        :param str fields: List of fields to provide as a partial response.
        :param str q: List of search terms.
        :param list[str] filters: List of filters to apply; for help with filters, click <a href=\"../mcs/howTo_Filters.html\">here</a>.
        :return: list[IUserModel]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['offset', 'limit', 'fields', 'q', 'filters']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method users_get" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []
        if 'offset' in params:
            query_params.append(('offset', params['offset']))  # noqa: E501
        if 'limit' in params:
            query_params.append(('limit', params['limit']))  # noqa: E501
        if 'fields' in params:
            query_params.append(('fields', params['fields']))  # noqa: E501
        if 'q' in params:
            query_params.append(('q', params['q']))  # noqa: E501
        if 'filters' in params:
            query_params.append(('filters', params['filters']))  # noqa: E501
            collection_formats['filters'] = 'multi'  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/vnd.phx.mcs-v1+json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Secured']  # noqa: E501

        return self.api_client.call_api(
            '/users', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='list[IUserModel]',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)
