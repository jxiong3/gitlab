# coding: utf-8

"""
    ModelCenter Cloud Swagger Client Library

    This is a test client library for ModelCenter Cloud. You can find out more about [ModelCenter Cloud Server API](https://10.10.14.75/execution/mcs/help.html) .        # noqa: E501

    OpenAPI spec version: 1.0.0
    Contact: jxiong@phoenix-int.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class IWorkflowMetadataModel(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'properties': 'object',
        'version': 'str',
        'description': 'str',
        'environment': 'object',
        'executable': 'str',
        'icon': 'str',
        'run_folder_preference': 'RunFolderPreference',
        'additional_values': 'object',
        'command_args': 'list[str]',
        'as_component': 'str',
        'requires': 'list[str]',
        'help_url': 'str',
        'instance_files': 'list[IInstanceFileModel]',
        'inputs': 'list[IVariableModel]',
        'outputs': 'list[IVariableModel]'
    }

    attribute_map = {
        'properties': 'properties',
        'version': 'version',
        'description': 'description',
        'environment': 'environment',
        'executable': 'executable',
        'icon': 'icon',
        'run_folder_preference': 'runFolderPreference',
        'additional_values': 'additionalValues',
        'command_args': 'commandArgs',
        'as_component': 'ASComponent',
        'requires': 'requires',
        'help_url': 'helpUrl',
        'instance_files': 'instanceFiles',
        'inputs': 'inputs',
        'outputs': 'outputs'
    }

    def __init__(self, properties=None, version=None, description=None, environment=None, executable=None, icon=None, run_folder_preference=None, additional_values=None, command_args=None, as_component=None, requires=None, help_url=None, instance_files=None, inputs=None, outputs=None):  # noqa: E501
        """IWorkflowMetadataModel - a model defined in Swagger"""  # noqa: E501

        self._properties = None
        self._version = None
        self._description = None
        self._environment = None
        self._executable = None
        self._icon = None
        self._run_folder_preference = None
        self._additional_values = None
        self._command_args = None
        self._as_component = None
        self._requires = None
        self._help_url = None
        self._instance_files = None
        self._inputs = None
        self._outputs = None
        self.discriminator = None

        if properties is not None:
            self.properties = properties
        if version is not None:
            self.version = version
        if description is not None:
            self.description = description
        if environment is not None:
            self.environment = environment
        if executable is not None:
            self.executable = executable
        if icon is not None:
            self.icon = icon
        if run_folder_preference is not None:
            self.run_folder_preference = run_folder_preference
        if additional_values is not None:
            self.additional_values = additional_values
        if command_args is not None:
            self.command_args = command_args
        if as_component is not None:
            self.as_component = as_component
        if requires is not None:
            self.requires = requires
        if help_url is not None:
            self.help_url = help_url
        if instance_files is not None:
            self.instance_files = instance_files
        if inputs is not None:
            self.inputs = inputs
        if outputs is not None:
            self.outputs = outputs

    @property
    def properties(self):
        """Gets the properties of this IWorkflowMetadataModel.  # noqa: E501

        The properties of the workflow  # noqa: E501

        :return: The properties of this IWorkflowMetadataModel.  # noqa: E501
        :rtype: object
        """
        return self._properties

    @properties.setter
    def properties(self, properties):
        """Sets the properties of this IWorkflowMetadataModel.

        The properties of the workflow  # noqa: E501

        :param properties: The properties of this IWorkflowMetadataModel.  # noqa: E501
        :type: object
        """

        self._properties = properties

    @property
    def version(self):
        """Gets the version of this IWorkflowMetadataModel.  # noqa: E501

        The version of the workflow  # noqa: E501

        :return: The version of this IWorkflowMetadataModel.  # noqa: E501
        :rtype: str
        """
        return self._version

    @version.setter
    def version(self, version):
        """Sets the version of this IWorkflowMetadataModel.

        The version of the workflow  # noqa: E501

        :param version: The version of this IWorkflowMetadataModel.  # noqa: E501
        :type: str
        """

        self._version = version

    @property
    def description(self):
        """Gets the description of this IWorkflowMetadataModel.  # noqa: E501

        The description of the workflow  # noqa: E501

        :return: The description of this IWorkflowMetadataModel.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this IWorkflowMetadataModel.

        The description of the workflow  # noqa: E501

        :param description: The description of this IWorkflowMetadataModel.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def environment(self):
        """Gets the environment of this IWorkflowMetadataModel.  # noqa: E501

        The environment of the workflow  # noqa: E501

        :return: The environment of this IWorkflowMetadataModel.  # noqa: E501
        :rtype: object
        """
        return self._environment

    @environment.setter
    def environment(self, environment):
        """Sets the environment of this IWorkflowMetadataModel.

        The environment of the workflow  # noqa: E501

        :param environment: The environment of this IWorkflowMetadataModel.  # noqa: E501
        :type: object
        """

        self._environment = environment

    @property
    def executable(self):
        """Gets the executable of this IWorkflowMetadataModel.  # noqa: E501

        The file name of the executable used by the workflow  # noqa: E501

        :return: The executable of this IWorkflowMetadataModel.  # noqa: E501
        :rtype: str
        """
        return self._executable

    @executable.setter
    def executable(self, executable):
        """Sets the executable of this IWorkflowMetadataModel.

        The file name of the executable used by the workflow  # noqa: E501

        :param executable: The executable of this IWorkflowMetadataModel.  # noqa: E501
        :type: str
        """

        self._executable = executable

    @property
    def icon(self):
        """Gets the icon of this IWorkflowMetadataModel.  # noqa: E501

        The file name of the icon used by the workflow  # noqa: E501

        :return: The icon of this IWorkflowMetadataModel.  # noqa: E501
        :rtype: str
        """
        return self._icon

    @icon.setter
    def icon(self, icon):
        """Sets the icon of this IWorkflowMetadataModel.

        The file name of the icon used by the workflow  # noqa: E501

        :param icon: The icon of this IWorkflowMetadataModel.  # noqa: E501
        :type: str
        """

        self._icon = icon

    @property
    def run_folder_preference(self):
        """Gets the run_folder_preference of this IWorkflowMetadataModel.  # noqa: E501


        :return: The run_folder_preference of this IWorkflowMetadataModel.  # noqa: E501
        :rtype: RunFolderPreference
        """
        return self._run_folder_preference

    @run_folder_preference.setter
    def run_folder_preference(self, run_folder_preference):
        """Sets the run_folder_preference of this IWorkflowMetadataModel.


        :param run_folder_preference: The run_folder_preference of this IWorkflowMetadataModel.  # noqa: E501
        :type: RunFolderPreference
        """

        self._run_folder_preference = run_folder_preference

    @property
    def additional_values(self):
        """Gets the additional_values of this IWorkflowMetadataModel.  # noqa: E501

        Reserved for future workflow configuration  # noqa: E501

        :return: The additional_values of this IWorkflowMetadataModel.  # noqa: E501
        :rtype: object
        """
        return self._additional_values

    @additional_values.setter
    def additional_values(self, additional_values):
        """Sets the additional_values of this IWorkflowMetadataModel.

        Reserved for future workflow configuration  # noqa: E501

        :param additional_values: The additional_values of this IWorkflowMetadataModel.  # noqa: E501
        :type: object
        """

        self._additional_values = additional_values

    @property
    def command_args(self):
        """Gets the command_args of this IWorkflowMetadataModel.  # noqa: E501

        The command line arguments of the executable used by the workflow  # noqa: E501

        :return: The command_args of this IWorkflowMetadataModel.  # noqa: E501
        :rtype: list[str]
        """
        return self._command_args

    @command_args.setter
    def command_args(self, command_args):
        """Sets the command_args of this IWorkflowMetadataModel.

        The command line arguments of the executable used by the workflow  # noqa: E501

        :param command_args: The command_args of this IWorkflowMetadataModel.  # noqa: E501
        :type: list[str]
        """

        self._command_args = command_args

    @property
    def as_component(self):
        """Gets the as_component of this IWorkflowMetadataModel.  # noqa: E501

        The analysis server component contained within the workflow  # noqa: E501

        :return: The as_component of this IWorkflowMetadataModel.  # noqa: E501
        :rtype: str
        """
        return self._as_component

    @as_component.setter
    def as_component(self, as_component):
        """Sets the as_component of this IWorkflowMetadataModel.

        The analysis server component contained within the workflow  # noqa: E501

        :param as_component: The as_component of this IWorkflowMetadataModel.  # noqa: E501
        :type: str
        """

        self._as_component = as_component

    @property
    def requires(self):
        """Gets the requires of this IWorkflowMetadataModel.  # noqa: E501

        The requirements needed to execute the workflow  # noqa: E501

        :return: The requires of this IWorkflowMetadataModel.  # noqa: E501
        :rtype: list[str]
        """
        return self._requires

    @requires.setter
    def requires(self, requires):
        """Sets the requires of this IWorkflowMetadataModel.

        The requirements needed to execute the workflow  # noqa: E501

        :param requires: The requires of this IWorkflowMetadataModel.  # noqa: E501
        :type: list[str]
        """

        self._requires = requires

    @property
    def help_url(self):
        """Gets the help_url of this IWorkflowMetadataModel.  # noqa: E501

        The help URL of the workflow  # noqa: E501

        :return: The help_url of this IWorkflowMetadataModel.  # noqa: E501
        :rtype: str
        """
        return self._help_url

    @help_url.setter
    def help_url(self, help_url):
        """Sets the help_url of this IWorkflowMetadataModel.

        The help URL of the workflow  # noqa: E501

        :param help_url: The help_url of this IWorkflowMetadataModel.  # noqa: E501
        :type: str
        """

        self._help_url = help_url

    @property
    def instance_files(self):
        """Gets the instance_files of this IWorkflowMetadataModel.  # noqa: E501

        The instance files contained within the workflow  # noqa: E501

        :return: The instance_files of this IWorkflowMetadataModel.  # noqa: E501
        :rtype: list[IInstanceFileModel]
        """
        return self._instance_files

    @instance_files.setter
    def instance_files(self, instance_files):
        """Sets the instance_files of this IWorkflowMetadataModel.

        The instance files contained within the workflow  # noqa: E501

        :param instance_files: The instance_files of this IWorkflowMetadataModel.  # noqa: E501
        :type: list[IInstanceFileModel]
        """

        self._instance_files = instance_files

    @property
    def inputs(self):
        """Gets the inputs of this IWorkflowMetadataModel.  # noqa: E501

        The input variables contained within the workflow  # noqa: E501

        :return: The inputs of this IWorkflowMetadataModel.  # noqa: E501
        :rtype: list[IVariableModel]
        """
        return self._inputs

    @inputs.setter
    def inputs(self, inputs):
        """Sets the inputs of this IWorkflowMetadataModel.

        The input variables contained within the workflow  # noqa: E501

        :param inputs: The inputs of this IWorkflowMetadataModel.  # noqa: E501
        :type: list[IVariableModel]
        """

        self._inputs = inputs

    @property
    def outputs(self):
        """Gets the outputs of this IWorkflowMetadataModel.  # noqa: E501

        The output variables contained within the workflow  # noqa: E501

        :return: The outputs of this IWorkflowMetadataModel.  # noqa: E501
        :rtype: list[IVariableModel]
        """
        return self._outputs

    @outputs.setter
    def outputs(self, outputs):
        """Sets the outputs of this IWorkflowMetadataModel.

        The output variables contained within the workflow  # noqa: E501

        :param outputs: The outputs of this IWorkflowMetadataModel.  # noqa: E501
        :type: list[IVariableModel]
        """

        self._outputs = outputs

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(IWorkflowMetadataModel, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, IWorkflowMetadataModel):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
