# coding: utf-8

"""
    ModelCenter Cloud Swagger Client Library

    This is a test client library for ModelCenter Cloud. You can find out more about [ModelCenter Cloud Server API](https://10.10.14.75/execution/mcs/help.html) .        # noqa: E501

    OpenAPI spec version: 1.0.0
    Contact: jxiong@phoenix-int.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class EventOutput(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'closed': 'bool',
        'type': 'ReflectType'
    }

    attribute_map = {
        'closed': 'closed',
        'type': 'type'
    }

    def __init__(self, closed=None, type=None):  # noqa: E501
        """EventOutput - a model defined in Swagger"""  # noqa: E501

        self._closed = None
        self._type = None
        self.discriminator = None

        if closed is not None:
            self.closed = closed
        if type is not None:
            self.type = type

    @property
    def closed(self):
        """Gets the closed of this EventOutput.  # noqa: E501


        :return: The closed of this EventOutput.  # noqa: E501
        :rtype: bool
        """
        return self._closed

    @closed.setter
    def closed(self, closed):
        """Sets the closed of this EventOutput.


        :param closed: The closed of this EventOutput.  # noqa: E501
        :type: bool
        """

        self._closed = closed

    @property
    def type(self):
        """Gets the type of this EventOutput.  # noqa: E501


        :return: The type of this EventOutput.  # noqa: E501
        :rtype: ReflectType
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this EventOutput.


        :param type: The type of this EventOutput.  # noqa: E501
        :type: ReflectType
        """

        self._type = type

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(EventOutput, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, EventOutput):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
