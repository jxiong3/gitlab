# coding: utf-8

"""
    ModelCenter Cloud Swagger Client Library

    This is a test client library for ModelCenter Cloud. You can find out more about [ModelCenter Cloud Server API](https://10.10.14.75/execution/mcs/help.html) .        # noqa: E501

    OpenAPI spec version: 1.0.0
    Contact: jxiong@phoenix-int.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class IVariableModel(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'name': 'str',
        'default_value': 'str',
        'format': 'str',
        'description': 'str',
        'type': 'str',
        'metadata': 'object',
        'enum_values': 'list[str]',
        'upper_bound': 'str',
        'enum_aliases': 'list[str]',
        'lower_bound': 'str',
        'units': 'str'
    }

    attribute_map = {
        'name': 'name',
        'default_value': 'defaultValue',
        'format': 'format',
        'description': 'description',
        'type': 'type',
        'metadata': 'metadata',
        'enum_values': 'enumValues',
        'upper_bound': 'upperBound',
        'enum_aliases': 'enumAliases',
        'lower_bound': 'lowerBound',
        'units': 'units'
    }

    def __init__(self, name=None, default_value=None, format=None, description=None, type=None, metadata=None, enum_values=None, upper_bound=None, enum_aliases=None, lower_bound=None, units=None):  # noqa: E501
        """IVariableModel - a model defined in Swagger"""  # noqa: E501

        self._name = None
        self._default_value = None
        self._format = None
        self._description = None
        self._type = None
        self._metadata = None
        self._enum_values = None
        self._upper_bound = None
        self._enum_aliases = None
        self._lower_bound = None
        self._units = None
        self.discriminator = None

        self.name = name
        if default_value is not None:
            self.default_value = default_value
        if format is not None:
            self.format = format
        if description is not None:
            self.description = description
        self.type = type
        if metadata is not None:
            self.metadata = metadata
        if enum_values is not None:
            self.enum_values = enum_values
        if upper_bound is not None:
            self.upper_bound = upper_bound
        if enum_aliases is not None:
            self.enum_aliases = enum_aliases
        if lower_bound is not None:
            self.lower_bound = lower_bound
        if units is not None:
            self.units = units

    @property
    def name(self):
        """Gets the name of this IVariableModel.  # noqa: E501

        The variable's name  # noqa: E501

        :return: The name of this IVariableModel.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this IVariableModel.

        The variable's name  # noqa: E501

        :param name: The name of this IVariableModel.  # noqa: E501
        :type: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def default_value(self):
        """Gets the default_value of this IVariableModel.  # noqa: E501

        The variable's default value  # noqa: E501

        :return: The default_value of this IVariableModel.  # noqa: E501
        :rtype: str
        """
        return self._default_value

    @default_value.setter
    def default_value(self, default_value):
        """Sets the default_value of this IVariableModel.

        The variable's default value  # noqa: E501

        :param default_value: The default_value of this IVariableModel.  # noqa: E501
        :type: str
        """

        self._default_value = default_value

    @property
    def format(self):
        """Gets the format of this IVariableModel.  # noqa: E501

        The variable's format  # noqa: E501

        :return: The format of this IVariableModel.  # noqa: E501
        :rtype: str
        """
        return self._format

    @format.setter
    def format(self, format):
        """Sets the format of this IVariableModel.

        The variable's format  # noqa: E501

        :param format: The format of this IVariableModel.  # noqa: E501
        :type: str
        """

        self._format = format

    @property
    def description(self):
        """Gets the description of this IVariableModel.  # noqa: E501

        The variable's description  # noqa: E501

        :return: The description of this IVariableModel.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this IVariableModel.

        The variable's description  # noqa: E501

        :param description: The description of this IVariableModel.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def type(self):
        """Gets the type of this IVariableModel.  # noqa: E501

        The variable's type, as used by the workflow  # noqa: E501

        :return: The type of this IVariableModel.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this IVariableModel.

        The variable's type, as used by the workflow  # noqa: E501

        :param type: The type of this IVariableModel.  # noqa: E501
        :type: str
        """
        if type is None:
            raise ValueError("Invalid value for `type`, must not be `None`")  # noqa: E501

        self._type = type

    @property
    def metadata(self):
        """Gets the metadata of this IVariableModel.  # noqa: E501

        The variable's metadata.  # noqa: E501

        :return: The metadata of this IVariableModel.  # noqa: E501
        :rtype: object
        """
        return self._metadata

    @metadata.setter
    def metadata(self, metadata):
        """Sets the metadata of this IVariableModel.

        The variable's metadata.  # noqa: E501

        :param metadata: The metadata of this IVariableModel.  # noqa: E501
        :type: object
        """

        self._metadata = metadata

    @property
    def enum_values(self):
        """Gets the enum_values of this IVariableModel.  # noqa: E501

        The variable's enumerated values; the set of possible values that can be assigned to the variable.  # noqa: E501

        :return: The enum_values of this IVariableModel.  # noqa: E501
        :rtype: list[str]
        """
        return self._enum_values

    @enum_values.setter
    def enum_values(self, enum_values):
        """Sets the enum_values of this IVariableModel.

        The variable's enumerated values; the set of possible values that can be assigned to the variable.  # noqa: E501

        :param enum_values: The enum_values of this IVariableModel.  # noqa: E501
        :type: list[str]
        """

        self._enum_values = enum_values

    @property
    def upper_bound(self):
        """Gets the upper_bound of this IVariableModel.  # noqa: E501

        The maximum allowable value of the variable; only allowed for numeric value types.  # noqa: E501

        :return: The upper_bound of this IVariableModel.  # noqa: E501
        :rtype: str
        """
        return self._upper_bound

    @upper_bound.setter
    def upper_bound(self, upper_bound):
        """Sets the upper_bound of this IVariableModel.

        The maximum allowable value of the variable; only allowed for numeric value types.  # noqa: E501

        :param upper_bound: The upper_bound of this IVariableModel.  # noqa: E501
        :type: str
        """

        self._upper_bound = upper_bound

    @property
    def enum_aliases(self):
        """Gets the enum_aliases of this IVariableModel.  # noqa: E501

        The alias names for the variable's enumerated values; the number of items in the alias list must match the number of items in the value list.  # noqa: E501

        :return: The enum_aliases of this IVariableModel.  # noqa: E501
        :rtype: list[str]
        """
        return self._enum_aliases

    @enum_aliases.setter
    def enum_aliases(self, enum_aliases):
        """Sets the enum_aliases of this IVariableModel.

        The alias names for the variable's enumerated values; the number of items in the alias list must match the number of items in the value list.  # noqa: E501

        :param enum_aliases: The enum_aliases of this IVariableModel.  # noqa: E501
        :type: list[str]
        """

        self._enum_aliases = enum_aliases

    @property
    def lower_bound(self):
        """Gets the lower_bound of this IVariableModel.  # noqa: E501

        The minimum allowable value of the variable; only allowed for numeric value types.  # noqa: E501

        :return: The lower_bound of this IVariableModel.  # noqa: E501
        :rtype: str
        """
        return self._lower_bound

    @lower_bound.setter
    def lower_bound(self, lower_bound):
        """Sets the lower_bound of this IVariableModel.

        The minimum allowable value of the variable; only allowed for numeric value types.  # noqa: E501

        :param lower_bound: The lower_bound of this IVariableModel.  # noqa: E501
        :type: str
        """

        self._lower_bound = lower_bound

    @property
    def units(self):
        """Gets the units of this IVariableModel.  # noqa: E501

        The variable's units  # noqa: E501

        :return: The units of this IVariableModel.  # noqa: E501
        :rtype: str
        """
        return self._units

    @units.setter
    def units(self, units):
        """Sets the units of this IVariableModel.

        The variable's units  # noqa: E501

        :param units: The units of this IVariableModel.  # noqa: E501
        :type: str
        """

        self._units = units

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(IVariableModel, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, IVariableModel):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
