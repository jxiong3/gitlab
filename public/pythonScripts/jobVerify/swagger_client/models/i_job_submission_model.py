# coding: utf-8

"""
    ModelCenter Cloud Swagger Client Library

    This is a test client library for ModelCenter Cloud. You can find out more about [ModelCenter Cloud Server API](https://10.10.14.75/execution/mcs/help.html) .        # noqa: E501

    OpenAPI spec version: 1.0.0
    Contact: jxiong@phoenix-int.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class IJobSubmissionModel(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'name': 'str',
        'cluster': 'str',
        'description': 'str',
        'queue': 'str',
        'trade_study_configuration': 'ITradeStudyConfigurationModel',
        'workflow_uri': 'str'
    }

    attribute_map = {
        'name': 'name',
        'cluster': 'cluster',
        'description': 'description',
        'queue': 'queue',
        'trade_study_configuration': 'tradeStudyConfiguration',
        'workflow_uri': 'workflowUri'
    }

    def __init__(self, name=None, cluster=None, description=None, queue=None, trade_study_configuration=None, workflow_uri=None):  # noqa: E501
        """IJobSubmissionModel - a model defined in Swagger"""  # noqa: E501

        self._name = None
        self._cluster = None
        self._description = None
        self._queue = None
        self._trade_study_configuration = None
        self._workflow_uri = None
        self.discriminator = None

        self.name = name
        self.cluster = cluster
        if description is not None:
            self.description = description
        self.queue = queue
        self.trade_study_configuration = trade_study_configuration
        self.workflow_uri = workflow_uri

    @property
    def name(self):
        """Gets the name of this IJobSubmissionModel.  # noqa: E501

        The name of the job  # noqa: E501

        :return: The name of this IJobSubmissionModel.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this IJobSubmissionModel.

        The name of the job  # noqa: E501

        :param name: The name of this IJobSubmissionModel.  # noqa: E501
        :type: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def cluster(self):
        """Gets the cluster of this IJobSubmissionModel.  # noqa: E501

        The cluster, by name, that the job will use for execution  # noqa: E501

        :return: The cluster of this IJobSubmissionModel.  # noqa: E501
        :rtype: str
        """
        return self._cluster

    @cluster.setter
    def cluster(self, cluster):
        """Sets the cluster of this IJobSubmissionModel.

        The cluster, by name, that the job will use for execution  # noqa: E501

        :param cluster: The cluster of this IJobSubmissionModel.  # noqa: E501
        :type: str
        """
        if cluster is None:
            raise ValueError("Invalid value for `cluster`, must not be `None`")  # noqa: E501

        self._cluster = cluster

    @property
    def description(self):
        """Gets the description of this IJobSubmissionModel.  # noqa: E501

        The description of the job  # noqa: E501

        :return: The description of this IJobSubmissionModel.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this IJobSubmissionModel.

        The description of the job  # noqa: E501

        :param description: The description of this IJobSubmissionModel.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def queue(self):
        """Gets the queue of this IJobSubmissionModel.  # noqa: E501

        The queue, by name, that the job will use for execution  # noqa: E501

        :return: The queue of this IJobSubmissionModel.  # noqa: E501
        :rtype: str
        """
        return self._queue

    @queue.setter
    def queue(self, queue):
        """Sets the queue of this IJobSubmissionModel.

        The queue, by name, that the job will use for execution  # noqa: E501

        :param queue: The queue of this IJobSubmissionModel.  # noqa: E501
        :type: str
        """
        if queue is None:
            raise ValueError("Invalid value for `queue`, must not be `None`")  # noqa: E501

        self._queue = queue

    @property
    def trade_study_configuration(self):
        """Gets the trade_study_configuration of this IJobSubmissionModel.  # noqa: E501


        :return: The trade_study_configuration of this IJobSubmissionModel.  # noqa: E501
        :rtype: ITradeStudyConfigurationModel
        """
        return self._trade_study_configuration

    @trade_study_configuration.setter
    def trade_study_configuration(self, trade_study_configuration):
        """Sets the trade_study_configuration of this IJobSubmissionModel.


        :param trade_study_configuration: The trade_study_configuration of this IJobSubmissionModel.  # noqa: E501
        :type: ITradeStudyConfigurationModel
        """
        if trade_study_configuration is None:
            raise ValueError("Invalid value for `trade_study_configuration`, must not be `None`")  # noqa: E501

        self._trade_study_configuration = trade_study_configuration

    @property
    def workflow_uri(self):
        """Gets the workflow_uri of this IJobSubmissionModel.  # noqa: E501

        The URI of the workflow the job will execute  # noqa: E501

        :return: The workflow_uri of this IJobSubmissionModel.  # noqa: E501
        :rtype: str
        """
        return self._workflow_uri

    @workflow_uri.setter
    def workflow_uri(self, workflow_uri):
        """Sets the workflow_uri of this IJobSubmissionModel.

        The URI of the workflow the job will execute  # noqa: E501

        :param workflow_uri: The workflow_uri of this IJobSubmissionModel.  # noqa: E501
        :type: str
        """
        if workflow_uri is None:
            raise ValueError("Invalid value for `workflow_uri`, must not be `None`")  # noqa: E501

        self._workflow_uri = workflow_uri

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(IJobSubmissionModel, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, IJobSubmissionModel):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
